# -*- coding: utf8 -*-
import math
import random
import numpy as np
import sys
#import matplotlib.pyplot as plt
import itertools


def dist(x):
    return math.e**(-0.5 * x**2)


def propose(x0, delta):
    return random.random() * 2 * delta + x0 - delta


def randomSeq(delta, length):
    x0 = 0
    series = [x0]
    while len(series) < length:
        x1 = propose(x0, delta)
        if random.random() < min(1, dist(x1) / dist(x0)):
            x0 = x1
        series.append(x0)
    return series


def autoCorrelation(seq, length):
    result = []
    square = np.mean(seq)**2
    var = np.var(seq)
    for k in range(length):
        w = []
        for i in range(len(seq)):
            if i + k >= len(seq):
                break
            w.append(seq[i] * seq[i+k])
        result.append((np.mean(w) - square) / var)
    return result


def fastAutoCor(seq, length):
    seq = np.asarray(seq)
    n = len(seq)
    var = np.var(seq)
    seq = seq - np.mean(seq)
    return np.asarray([(seq[:n-k] * seq[-(n-k):]).mean() for k in range(length)]) \
        / var


def correlation(seq, k, var):
    n = len(seq)
    var = np.var(seq)
    seq = seq - np.mean(seq)
    return (seq[:n-k] * seq[-(n-k):]).mean() / var


def newAutoCor(seq):
    n = len(seq)
    var = np.var(seq)
    seq = seq - np.mean(seq)
    r = np.correlate(seq, seq, mode="full")[-n:]
    return r / (var * np.arange(n, 0, -1))


def getCor(seq, delta):
    var = np.var(seq)
    with open("%s.txt" % str(delta), "a") as f:
        for i in itertools.count(0):
            cor = correlation(seq, i, var)
            print i, cor
            f.write("%d %f\n" % (i, cor))


def getSpecificCor(delta):
    length = int(2e6)
    seq = randomSeq(delta, length)
    print "seqed"
    getCor(seq, delta)


def sweepDelta(delta):
    length = int(2e6)
    #for delta in np.arange(0.1, 10, 0.1):
        #print delta
    seq = randomSeq(delta, length)
    var = np.var(seq)
    for i in range(2000):
        if abs(correlation(seq, i, var)) < 0.01:
            break
    with open("sweep.txt", "a") as f:
        f.write("%f %d\n" % (delta, i))

if __name__ == "__main__":
    _, delta = sys.argv
    sweepDelta(float(delta))
