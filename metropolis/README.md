# 简介

本文简单地介绍了 Metropolis 算法的原理和实现。

主要内容在`metropolis.ipynb`这个 Ipython notebook 中，建议使用最新版打开。`metropolis.pdf`则是将前面的ipynb转换成pdf文档的结果，方便阅读。
