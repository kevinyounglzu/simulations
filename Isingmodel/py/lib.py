# -*- coding: utf8 -*-
import igraph as ig
import numpy as np
import random
import math


class Ising(object):
    def __init__(self, size, T):
        self.size = size
        self.lattice = ig.Graph.Lattice([self.size] * 2)
        self.num = len(self.lattice.vs)

        self.t = float(T)

        for vertice in self.lattice.vs:
            #vertice["state"] = 1
            vertice["state"] = random.choice([-1, 1])

        self.energy = self.getEnergy()
        self.averageS = self.getAverageS()

    def simu(self, step):
        result = []
        for i in range(step):
            result.append((self.averageS, self.energy))
            self.oneTrial()
        return result

    def drySimu(self, step):
        for i in range(step):
            self.oneTrial()

    def oneTrial(self):
        index = random.randint(0, self.num - 1)
        #print index
        delta_energy = self._deltaEnergy(index)
        #print delta_energy
        #print math.exp(- delta_energy / self.t)
        if random.random() < math.exp(- delta_energy / self.t):
            #print "hello"
            self.averageS -= 2 * self.lattice.vs[index]["state"] / float(self.num)
            self.lattice.vs[index]["state"] = - self.lattice.vs[index]["state"]
            self.energy += delta_energy

    def show(self):
        print [v["state"] for v in self.lattice.vs]

    def _deltaEnergy(self, index):
        target = self.lattice.vs[index]
        return 2 * target["state"] * sum([v["state"] for v in target.neighbors()])

    def getEnergy(self):
        return sum(- self.lattice.vs[e.source]["state"]
                   * self.lattice.vs[e.target]["state"]
                   for e in self.lattice.es)

    def getAverageS(self):
        return np.mean([v["state"] for v in self.lattice.vs])


if __name__ == "__main__":
    step = int(1e7)
    ising = Ising(40, 2.2)
    #r = ising.simu(step)
    ising.drySimu(step)
