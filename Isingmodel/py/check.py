# -*- coding: utf8 -*-
import numpy as np
import os
from StringIO import StringIO
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


def yieldData():
    base = "./data/"
    file_names = os.listdir(base)
    for i, file_name in enumerate(file_names):
        if file_name.endswith("txt"):
            with open(base + file_name) as f:
                a = f.readlines()
            s = "".join(a[:-1])
            yield file_name, np.loadtxt(StringIO(s))


def yieldD(file_name):
    x = float(file_name.split("/")[-1][:-4])
    a = np.loadtxt(file_name)

    s = np.abs(a[:, 0])
    e = a[:, 1]

    return x, np.mean(s), np.var(s), np.mean(e), np.var(e)


def stat(L):
    #gen = yieldD()
    base = "./data/aver/%d/" % L
    file_names = [base + f for f in os.listdir(base) if f.endswith("txt")]
    result = []
    for i, f in enumerate(file_names):
        print i
        result.append(yieldD(f))
    a = np.asarray(result)
    np.savetxt(base + "result.dat", a)

def correlation():
    base = "./data/cor/40/"
    file_names = [base + f for f in os.listdir(base) if f.endswith("txt")]
    xs = []
    ys = []
    zs = []
    for f in file_names:
        x =  float(f[14: 19])
        a = np.loadtxt(f)
        for j, num in enumerate(a[2:]):
            xs.append(x)
            ys.append(j)
            zs.append(num)
        #plt.plot(np.arange(len(a) - 2), a[2:], label=f)
    fig = plt.figure()
    ax = fig.add_subplot(111, projection="3d")
    ax.scatter(xs, ys, zs, edgecolor="none")
    #plt.scatter(np.arange(len(result)), result)
    plt.show()

def binder():
    base = "./data/20/"
    file_names = [base + f for f in os.listdir(base) if f.endswith("txt")]

    u = []
    for i, file_name in enumerate(file_names):
        print i
        x = float(file_name.split("/")[-1][:-4])
        a = np.loadtxt(file_name)

        s = np.abs(a[:, 0])
        u.append([x, 1 - np.mean(s**4) / (3 * np.mean(s**2)**2)])
    u = np.asarray(u)
    np.savetxt(base + "binder.dat", u)

if __name__ == "__main__":
    #correlation()
    #binder()
    for i in [8, 16]:
        stat(i)
