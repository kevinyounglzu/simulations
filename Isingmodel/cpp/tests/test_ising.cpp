#include <iostream>
#include "Ising.h"
#include <gtest/gtest.h>

using namespace std;

TEST(Test_Spin, Test_Spin_Setting)
{
    Spin new_spin_1(1);
    EXPECT_EQ(new_spin_1.getSpin(), 1) << "Spin not 1.";

    new_spin_1.setSpin(-1);
    EXPECT_EQ(new_spin_1.getSpin(), -1) << "Spin not -1.";

    Spin new_spin_2(-1);
    EXPECT_EQ(new_spin_2.getSpin(), -1) << "Spin not -1.";

    Spin wrong_intentionally(0);
    EXPECT_EQ(wrong_intentionally.getSpin(), 1) << "Spin not 1.";
}

TEST(Test_Spin, TestFlip)
{
    Spin up_spin(1);
    EXPECT_EQ(up_spin.getSpin(), 1) << "Spin not 1.";
    up_spin.flipSpin();
    EXPECT_EQ(up_spin.getSpin(), -1) << "Spin not -1";

    Spin down_spin(-1);
    EXPECT_EQ(down_spin.getSpin(), -1) << "Spin not -1";
    down_spin.flipSpin();
    EXPECT_EQ(down_spin.getSpin(), 1) << "Spin not 1.";
}

TEST(Test_Ising, Test_Attr_methods)
{
    int length(10);
    double T(200.);
    Ising is(length, T);

    EXPECT_EQ(is.getSize(), length*length);
    EXPECT_EQ(is.getT(), T);
}

TEST(Test_Ising, Test_Initializition)
{
    int length(10);
    double T(200.);
    Ising is(length, T);

    int n;
    for(int i=0; i<is.getSize(); i++)
    {
        n = is[i].getSpin();
        EXPECT_TRUE(n==-1 || n==1);
    }
}

TEST(Test_Ising, Test_Iterators)
{
    /* Test empty */
    Ising is_emtpy(0, 200.);
    EXPECT_EQ(is_emtpy.begin(), is_emtpy.end());

    /* Test full */
    Ising is(10, 200.);
    int i(0);
    for(auto iter=is.begin(); iter != is.end(); ++iter)
    {
        EXPECT_EQ(&is[i], &(*iter));
        ++i;
    }
}

TEST(Test_Ising, Test_Utilities)
{
    Ising is(100, 200.);
    cout << is.getAverageSpin() << endl;
}

int main(int argc, char* argv[])
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
