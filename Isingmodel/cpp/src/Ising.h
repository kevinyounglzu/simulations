#ifndef ISING_H
#define ISING_H
#include "graph.h"
#include <iostream>
#include <vector>
#include <list>
#include "rand.h"

using namespace mylib;
using namespace std;


/* Class for the Spin on the lattice. */
class Spin
{
    private:
        int _spin_value; // The spin of the node, only has value 1 or -1.
    public:
        Spin(int spin_value);
        /* Attr methods */
        int getSpin() { return _spin_value; }
        int setSpin(int spin_value);
        void flipSpin() { _spin_value = - _spin_value; }
};

class IsingIter; // forward deleration
/* The Ising object for simulation */
class Ising
{
    private:
        int size; // the total spin of the system
        double _T; // the temperature
        list<Spin> lis; 
        TwoDLattice<Spin> lattice; // the lattice to hold the spins

    public:
        /* Constructor */
        Ising(int length, double T);
        /* Attr methods */
        int getSize() { return size; }
        double getT() { return _T; }
        /* Utilities for simulation */
        double getAverageSpin();
        /* Overloaded operators */
        Spin & operator[](const int index);
        /* For iterators. */
        IsingIter begin();
        IsingIter end();
};

/* Iterator for Ising class. */
class IsingIter
{
    typedef Spin           value_type;
    typedef value_type *   pointer; 
    typedef value_type &   reference;

    private:
        Ising & _is; // the ising model of the iterator
        int _index; //  the index of the spin this iteraor points to
        pointer ptr;
    
    public:
        IsingIter(Ising & is, int index);
        IsingIter(Ising & is);
        reference operator*() const { return *ptr; }
        pointer operator->() const { return ptr; }
        IsingIter& operator++();
        IsingIter operator++(int);
        bool operator==(const IsingIter& other) const { return ptr == other.ptr; }
        bool operator!=(const IsingIter& other) const { return ptr != other.ptr; }
};


/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * implematations for the classes.*/

/* For  Spin */
Spin::Spin(int spin_value)
{
    if(!setSpin(spin_value))
    {
        _spin_value = 1;
        std::cout << "Set spin to default value 1.";
    }
}

int Spin::setSpin(int spin_value)
{
    if(spin_value == 1 || spin_value == -1)
    {
        _spin_value = spin_value;
        return 1;
    }
    else
    {
        std::cout << "Spin not 1 or -1, dose not change.";
        return 0;
    }
}

/* For Ising */
Ising::Ising(int length, double T): size(length*length), _T(T), lattice(length)
{
    Spin s(1);
    vector<int> choices = {-1, 1};
    RandChoice<int> randch(choices);

    for(int i=0; i<lattice.getSize(); i++)
    {
        s.setSpin(randch.choose());
        lis.push_back(s);
        lattice[i].content = &lis.back();
    }
}

double Ising::getAverageSpin()
{
    int sum_of_spin(0);
    for(auto & spin : *this)
    {
        sum_of_spin += spin.getSpin();
    }
    return static_cast<double>(sum_of_spin) / static_cast<double>(size);
}

Spin & Ising::operator[](const int index)
{
    return *(lattice[index].content);
}

/* For iterators. */
IsingIter Ising::begin()
{
    if(size > 0)
        return IsingIter(*this, 0);
    else
        return IsingIter(*this);
}

IsingIter Ising::end()
{
    IsingIter iter(*this);
    return iter;
}

/* For IsingIter */
IsingIter::IsingIter(Ising & is, int index): _is(is), _index(index), ptr(&is[index])
{
}
/* For end() iterator */
IsingIter::IsingIter(Ising & is): _is(is), _index(-1),ptr(0)
{
}

IsingIter& IsingIter::operator++()
{
    if(_index < _is.getSize() - 1)
    {
        _index++;
        ptr = &_is[_index];
    }
    else
    {
        _index = -1;
        ptr = 0;
    }
    return *this;
}

IsingIter IsingIter::operator++(int)
{
    IsingIter temp = *this;
    ++*this;
    return temp;
}
#endif
