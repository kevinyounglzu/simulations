#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

#define IsingAttrs \
    int L; \
    int size; \
    int energy; \
    int * lattice; \
    int ** neighbors; \
    double averS; \
    double t

//    int L;  The length of the lattice.
//    int size; L*L
//    int energy; The energy of the system
//    int * lattice; A array of integer is used to represent the lattice's states
//    int ** neighbors; The index of the neighbors of every node
//    double averS; <s>
//    double t The tempreture of the system

/* The structure of Ising model */
typedef struct Ising {
    IsingAttrs;
} Ising;

/* The structure of Ising model with utility to calculate correlation */
typedef struct IsingCor {
    IsingAttrs;
    int * aver;
    int ** cor;
} IsingCor;

/* The constrction and destruction function of Ising */
Ising * Ising_create(int L, double t);
void Ising_destroy(Ising * model);

/* The constrction and destruction function of IsingCor */
IsingCor * IsingCor_create(int L, double t);
void IsingCor_destroy(IsingCor * model);

/* Utilities. */
int Ising_get_energy(Ising * model);
double Ising_get_average_S(Ising * model);
void IsingCor_set_cor(IsingCor * model);

/* simulation utility */
int Ising_one_trial(Ising *model);

int IsingCor_one_trial(IsingCor * model);
int IsingCor_stat(IsingCor *model, int samples, double result[]);

/* Helper functions */
double Ising_delta_energy(Ising * model, int index);
int get_hamming(int L, int i, int j);
int * find_neighbors(int size, int index);

/* Generate a random number in [0, 1) */
static inline double generateRandom(void)
{
    return (rand() - 1) / (double)RAND_MAX;
}

/* Generate 1 and -1 both with possibility 0.5. */
static inline int randomOne(void)
{
    if(generateRandom() < 0.5)
    {
        return -1;
    }
    else
    {
        return 1;
    }
}

/* generate an random integer r so that 0 <= r <= upperbound */
static inline int generateRandint(int upperbound)
{
    return rand() % upperbound + 1;
}
