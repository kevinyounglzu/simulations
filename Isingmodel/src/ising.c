#include "ising.h"

/* Some private functions. */
int Ising_init(Ising * model, int L, double t);
void Ising_clear(Ising * model);

/* Allocate the memory to create Ising. */
Ising * Ising_create(int L, double t)
{

    Ising * model = malloc(sizeof(Ising));
    Ising_init(model, L, t);

    return model;
}

/* Free the allocated memory. */
void Ising_destroy(Ising * model)
{
    Ising_clear(model);

    free(model);
}

/* Free the attributes of Ising model. */
void Ising_clear(Ising * model)
{
    int i;

    if(model->lattice)
    {
        free(model->lattice);
    }

    if(model->neighbors)
    {
        for(i=0; i<model->size; i++)
        {
            if(model->neighbors[i])
            {
                free(model->neighbors[i]);
            }
        }

        free(model->neighbors);
    }
}

/*  Allocate the memory to create IsingCor */
IsingCor * IsingCor_create(int L, double t)
{
    int i, j;

    IsingCor * model = malloc(sizeof(IsingCor));

    Ising_init((Ising *)model, L, t);

    model->aver = calloc(model->size, sizeof(int));
    for(i=0; i<model->size; i++)
    {
        model->aver[i] = 0;
    }

    model->cor = calloc(model->size, sizeof(int *));
    for(i=0; i<model->size; i++)
    {
        model->cor[i] = calloc(model->size, sizeof(int));
        for(j=0; j<model->size; j++)
        {
            model->cor[i][j] = 0;
        }
    }

    return model;
}

/* Free the allocated memory. */
void IsingCor_destroy(IsingCor * model)
{
    int i;

    if(model->cor)
    {
        for(i=0; i<model->size; i++)
        {
            if(model->cor[i])
            {
                free(model->cor[i]);
            }
        }
        free(model->cor);
    }
    
    if(model->aver)
    {
        free(model->aver);
    }

    Ising_clear((Ising *)model);

    free(model);
}

/* Initialize Ising. */
int Ising_init(Ising * model, int L, double t)
{
    int i;

    model->L = L;
    model->size = L * L;
    model->t = t;

    model->lattice = calloc(model->size, sizeof(int));
    for(i=0; i<model->size; i++)
    {
        model->lattice[i] = randomOne();
    }

    model->neighbors = calloc(model->size, sizeof(int *));
    for(i=0; i<model->size; i++)
    {
        model->neighbors[i] = find_neighbors(model->L, i);
    }

    model->energy = Ising_get_energy(model);
    model->averS = Ising_get_average_S(model);

    return 1;
}

int Ising_get_energy(Ising * model)
{
    int i, j;
    int state;
    int energy;

    energy = 0;
    for(i=0; i<model->size; i++)
    {
        state = model->lattice[i];
        for(j=0; j<4; j++)
        {
            energy = energy - state * model->lattice[model->neighbors[i][j]];
        }
    }

    return energy / 2;
}

double Ising_get_average_S(Ising * model)
{
    int num_of_s = 0;
    int i;

    for(i=0; i<model->size; i++)
    {
        num_of_s += model->lattice[i];
    }

    return num_of_s / (double)model->size;
}

void IsingCor_set_cor(IsingCor * model)
{
    int i, j;

    for(i=0; i<model->size; i++)
    {
        for(j=0; j<model->size; j++)
        {
            model->cor[i][j] += model->lattice[i] * model->lattice[j];
        }
        model->aver[i] += model->lattice[i];
    }
}

int Ising_one_trial(Ising *model)
{
    /* Choose the index of the node to flip randomly. */
    int index = generateRandint(model->size - 1);
    /* Calculate the ennergy change due to the flip. */
    double de = Ising_delta_energy(model, index);

    if(de < 0)
    {
        /* Flip the node. */
        model->averS -= 2 * model->lattice[index] / (double)model->size;
        model->lattice[index] = - model->lattice[index];
        model->energy += de;
        return 1;
    }
    else
    {
        if(generateRandom() < exp(- de / model->t))
        {
            model->averS -= 2 * model->lattice[index] / (double)model->size;
            model->lattice[index] = - model->lattice[index];
            model->energy += de;
            return 1;
        }
        else
        {
            return 0;
        }
    }

//    set_cor(model);
}

int IsingCor_one_trial(IsingCor * model)
{
    int index = generateRandint(model->size - 1);
    double de = Ising_delta_energy((Ising *)model, index);

    if(de < 0)
    {
        /* Flip the node. */
        model->lattice[index] = - model->lattice[index];
        return 1;
    }
    else
    {
        if(generateRandom() < exp(- de / model->t))
        {
            model->lattice[index] = - model->lattice[index];
            return 1;
        }
        else
        {
            return 0;
        }
    }
}

int IsingCor_stat(IsingCor *model, int samples, double result[])
{
    int i, j;
    double n;
    int distance = 0;

    n = (double)samples;

    int * counter = malloc(model->L * sizeof(int));


    for(i=0; i<model->L; i++)
    {
        result[i] = 0;
        counter[i] = 0;
    }

    for(i=0; i<model->size; i++)
    {
        for(j=0; j<model->size; j++)
        {
            distance = get_hamming(model->L, i, j) ;
//            printf("%d %d %d\n", i, j, distance);
            result[distance] += (model->cor[i][j] - model->aver[i] * model->aver[j] / n) / n ;
            counter[distance] += 1;
        }
    }
//    printf("hye\n");

    for(i=0; i<model->L; i++)
    {
//        printf("%f\n", result[i]);
        result[i] = result[i] / (float)counter[i];
//        printf("%d\n", counter[i]);
    }

    return 1;
}

double Ising_delta_energy(Ising * model, int index)
{
    int sum_of_states=0;
    int i;

    for(i=0; i<4; i++)
    {
        sum_of_states += model->lattice[model->neighbors[index][i]];
    }

    return 2 * model->lattice[index] * sum_of_states;
}

int get_hamming(int L, int i, int j)
{
    int i_x = i % L;
    int i_y = i / L;

    int j_x = j % L;
    int j_y = j / L;

    int delta_x = abs(i_x - j_x);
    int delta_y = abs(i_y - j_y);

    if(delta_x > L / 2)
    {
        delta_x = L - delta_x;
    }

    if(delta_y > L / 2)
    {
        delta_y = L - delta_y;
    }

    return delta_x + delta_y;
}

/* Implement the perodic boundry */
int * find_neighbors(int L, int index)
{
    int * buffer = calloc(4, sizeof(int));
    int x, y;
    int n_x, n_y;

    x = index % L;
    y = index / L;

    /* left */
    if(x==0)
    {
        n_x = L - 1;
    }
    else
    {
        n_x = x - 1;
    }
    n_y = y;
    buffer[0] = n_y * L + n_x;

    /* up */
    if(y==0)
    {
        n_y = L - 1;
    }
    else
    {
        n_y = y - 1;
    }
    n_x = x;
    buffer[1] = n_y * L + n_x;

    /* right */
    if(x==L-1)
    {
        n_x = 0;
    }
    else
    {
        n_x = x + 1;
    }
    n_y = y;
    buffer[2] = n_y * L + n_x;

    /* down */
    if(y==L-1)
    {
        n_y = 0;
    }
    else
    {
        n_y = y + 1;
    }
    n_x = x;
    buffer[3] = n_y * L + n_x;

    return buffer;
}

void show_model(Ising * model)
{
    int i;
    for(i=0; i<model->size; i++)
    {
        printf("%d ", model->lattice[i]);
    }
    printf(" E: %d", model->energy);
    printf("\n");
}
