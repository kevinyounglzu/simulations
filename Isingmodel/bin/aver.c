/*
 * =====================================================================================
 *
 *       Filename:  simulation.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  06/21/2015 19:59:09
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "ising.h"

int main (int argc, char *argv[])
{
    int L = atoi(argv[1]);
    double t = atof(argv[2]);
    int i = 0;
    int j = 0;
    int samples = 1000000;
    int mc_step = L * L ;
    int step = mc_step * 1000;
    FILE *fp;
    char file_name[100];

    srand(time(NULL));
    generateRandom();
    generateRandom();
    generateRandom();

    Ising * model = Ising_create(L, t);

    /* Begin the simulation. Discard the transition phase. */
    for(i=0; i<step * 20; i++)
    {
        Ising_one_trial(model);
    }

    /* Open file. */
    sprintf(file_name, "./data/aver/%d/%f.txt", L, t);
    fp = fopen(file_name, "w");

    /* Begin sampling. */
    for(i=0; i<samples; i++)
    {
//        printf("%d\n", i);
        for(j=0; j<mc_step; j++)
        {
            Ising_one_trial(model);
        }
//        printf("%f %d\n", model->averS, model->energy);
        fprintf(fp, "%f %d\n", model->averS, model->energy);
    }
    fclose(fp);

    Ising_destroy(model);

    return EXIT_SUCCESS;
}
/* ----------  end of function main  ---------- */
