# -*- coding: utf8 -*-
import numpy as np

def dump(config):
    with open("config", "w") as f:
        for s, i in config:
            f.write("%d %f\n" % (s, i))

def sweep():
    size = [20, 40, 80, 160]
    a = [np.linspace(1, 1.9, 10), np.linspace(2, 2.4, 41), np.linspace(2.5, 3.5, 11)]
    with open("config", "w") as f:
        for s in size:
            for l in a:
                for i in l:
                    f.write("%d %f\n" % (s, i))

def sweep2():
    with open("config", "w") as f:
        size = [160]
        a = [np.linspace(1, 1.9, 10), np.linspace(2, 2.4, 41), np.linspace(2.5, 3.5, 11)]
        for s in size:
            for l in a:
                for i in l:
                    f.write("%d %f\n" % (s, i))
        size = [80]
        a = [np.linspace(1, 1.9, 10)]
        for s in size:
            for l in a:
                for i in l:
                    f.write("%d %f\n" % (s, i))

if __name__ == "__main__":
    sizes = [4, 8, 16, 20, 40, 80]
    config = []
    for size in sizes:
        #for param in np.linspace(1, 3.5):
        for param in [2.268, 2.269, 2.270]:
            config.append([size, param])
    dump(np.asarray(config))
