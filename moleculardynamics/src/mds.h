#include <stdio.h>
#include <stdlib.h>

/* Some parameters */
#define Epsilon 1
#define Sigma 1
#define L 10
#define M 1
#define Delta_t 0.001
#define V0 1
#define N 100
#define STEPS 10000

/* Data structure for moleculars. */
typedef struct Molecular {
    double x; /* X cordinate. */
    double y; /* Y cordinate. */
    double vx; /* Velocity alonge x axis. */
    double vy; /* Velocity alonge y axis. */
    double force_x; /* The sum of forces along x axis on this molecular. */
    double force_y; /* The sum of forces along y axis on this molecular. */
    /* Integretion requires the force of last time step. */
    double force_x_old; /* Force of last time step. */
    double force_y_old; /* Force of last time step. */
} Molecular;

/* A array of Molecular objects. */
typedef struct Mollist {
    int length; /* Length of this array. */
    Molecular ** array;
    double kinetic_energy;
    double potential_energy;
} Mollist;

/* A structure for data encapusalation. */
typedef struct Distance {
    double delta_x;
    double delta_y;
    /* For the calculation of sqrt is very expensive, use r^2 instead. */
    double rsqure;
} Distance;

/* Utility for dynamical memory management. */
Molecular * Molecular_create();
void Molecular_destroy(Molecular * m);
Mollist * Mollist_create(int length);
void Mollist_destroy(Mollist * moll);

/* Initialize the Mollist with random distribution. */
int Mollist_init(Mollist * moll);

/* Utilities for find the distance between two moleculars. */
void find_distance(Molecular * m1, Molecular * m2, Distance * dis);

/* Potential funciton */
double lennord_jones_force(double r);
double lennord_jones_potential(double r);

/* Integretion method. */
double velocity_verlet_position(double velocity, double accceleration);
double velocity_verlet_velocity(double old_a, double new_a);

/* Update positions and velocities. */
void update(Mollist * moll);

/* Some random functions. */
/* Generate a random number in [0, 1) */
static inline double generateRandom(void)
{
    return (rand() - 1) / (double)RAND_MAX;
}

/* Generate a random number in [lower_bound, upper_bound) */
static inline double generateRandomR(int lower_bound, int upper_bound)
{
    return generateRandom() * (upper_bound - lower_bound) + lower_bound;
}
