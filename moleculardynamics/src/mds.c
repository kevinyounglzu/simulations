#include "mds.h"
#include <math.h>

Molecular * Molecular_create()
{
    Molecular * m = malloc(sizeof(Molecular));
    m->x = 0;
    m->y = 0;
    m->vx = 0;
    m->vy = 0;

    m->force_x = 0;
    m->force_y = 0;
    m->force_x_old = 0;
    m->force_y_old = 0;

    return m;
}

void Molecular_destroy(Molecular * m)
{
    if(NULL != m)
    {
        free(m);
    }
}

Mollist * Mollist_create(int length)
{
    Mollist * moll = malloc(sizeof(Mollist));
    moll->length = length;
    moll->array = calloc(length, sizeof(Molecular *));

    moll->kinetic_energy = 0;
    moll->potential_energy = 0;

    for(int i=0; i<moll->length; i++)
    {
        moll->array[i] = Molecular_create();
    }

    return moll;
}

void Mollist_destroy(Mollist * moll)
{
    if(NULL != moll)
    {
        if(NULL != moll->array)
        {
            for(int i=0; i<moll->length; i++)
            {
                Molecular_destroy(moll->array[i]);
            }
            free(moll->array);
        }

        free(moll);
    }
}

int Mollist_init(Mollist * moll)
{
    int i, j;
    double f, fx, fy;
    int x, y;
    double xx, yy;
    double r;
    Molecular * m;
    Distance dis;

    dis.delta_x = 0;
    dis.delta_y = 0;
    dis.rsqure = 0;

    /* Initialize the positions and velocities. 
     * The moleculars are distributed on a lattice of the plane.
     * The abusolute velocity equals V0, but the dirctions are randomly distributed.
     * */
    for(i=0; i<moll->length; i++)
    {
        m = moll->array[i];
        x = i % 10;
        y = i / 10;
        m->x = x * L / 11.;
        m->y = y * L / 11.;

        xx = generateRandom();
        yy = generateRandom();
        m->vx = V0 * yy / sqrt(xx * xx + yy * yy);
        m->vy = V0 * xx / sqrt(xx * xx + yy * yy);

        moll->kinetic_energy = moll->length * 0.5 * M * V0 * V0;

        m->force_x = 0;
        m->force_x_old = 0;

        m->force_y = 0;
        m->force_y_old = 0;
    }

    /* Calculate the forces and portential energy of the system. */
    for(i=0; i<moll->length; i++)
    {
        for(j=i+1; j<moll->length; j++)
        {
            find_distance(moll->array[i], moll->array[j], &dis);
            r = sqrt(dis.rsqure);
            f = lennord_jones_force(r);
            moll->potential_energy += lennord_jones_potential(r);

            fx = f * dis.delta_x / dis.rsqure;
            fy = f * dis.delta_y / dis.rsqure;

            moll->array[i]->force_x_old += fx;
            moll->array[i]->force_y_old += fy;

            moll->array[j]->force_x_old -= fx;
            moll->array[j]->force_y_old -= fy;
        }
    }
    return 1;
}

/* Calculate the distance of two given moleculars, the result is restored in a Distance object.
 * Be carefull of the perodic boundry.
 * */
void find_distance(Molecular * m1, Molecular * m2, Distance * dis)
{
    double distance_square;
    double delta_x, delta_y;
    
    /* Implement the perodic boundry. */
    /* Upper left. */
    delta_x = (m2->x - L) - m1->x;
    delta_y = (m2->y + L) - m1->y;
    dis->rsqure = delta_x * delta_x + delta_y * delta_y;
    dis->delta_x = delta_x;
    dis->delta_y = delta_y;

    /* Upper. */
    delta_x = m2->x - m1->x;
    delta_y = (m2->y + L) - m1->y;
    distance_square = delta_x * delta_x + delta_y * delta_y;
    if(distance_square < dis->rsqure)
    {
        dis->rsqure = distance_square;
        dis->delta_x = delta_x;
        dis->delta_y = delta_y;
    }

    /* Upper right. */
    delta_x = (m2->x + L) - m1->x;
    delta_y = (m2->y + L) - m1->y;
    distance_square = delta_x * delta_x + delta_y * delta_y;
    if(distance_square < dis->rsqure)
    {
        dis->rsqure = distance_square;
        dis->delta_x = delta_x;
        dis->delta_y = delta_y;
    }

    /* Left. */
    delta_x = (m2->x - L) - m1->x;
    delta_y = m2->y - m1->y;
    distance_square = delta_x * delta_x + delta_y * delta_y;
    if(distance_square < dis->rsqure)
    {
        dis->rsqure = distance_square;
        dis->delta_x = delta_x;
        dis->delta_y = delta_y;
    }

    /* Center. */
    delta_x = m2->x - m1->x;
    delta_y = m2->y - m1->y;
    distance_square = delta_x * delta_x + delta_y * delta_y;
    if(distance_square < dis->rsqure)
    {
        dis->rsqure = distance_square;
        dis->delta_x = delta_x;
        dis->delta_y = delta_y;
    }

    /* Right. */
    delta_x = (m2->x + L) - m1->x;
    delta_y = m2->y - m1->y;
    distance_square = delta_x * delta_x + delta_y * delta_y;
    if(distance_square < dis->rsqure)
    {
        dis->rsqure = distance_square;
        dis->delta_x = delta_x;
        dis->delta_y = delta_y;
    }

    /* Bottom left. */
    delta_x = (m2->x - L) - m1->x;
    delta_y = (m2->y - L) - m1->y;
    distance_square = delta_x * delta_x + delta_y * delta_y;
    if(distance_square < dis->rsqure)
    {
        dis->rsqure = distance_square;
        dis->delta_x = delta_x;
        dis->delta_y = delta_y;
    }

    /* Bottom. */
    delta_x = m2->x - m1->x;
    delta_y = (m2->y - L) - m1->y;
    distance_square = delta_x * delta_x + delta_y * delta_y;
    if(distance_square < dis->rsqure)
    {
        dis->rsqure = distance_square;
        dis->delta_x = delta_x;
        dis->delta_y = delta_y;
    }

    /* Bottom right. */
    delta_x = (m2->x + L) - m1->x;
    delta_y = (m2->y - L) - m1->y;
    distance_square = delta_x * delta_x + delta_y * delta_y;
    if(distance_square < dis->rsqure)
    {
        dis->rsqure = distance_square;
        dis->delta_x = delta_x;
        dis->delta_y = delta_y;
    }
}

/* Potential funciton. */
double lennord_jones_force(double r)
{
    return 24 * Epsilon * (pow(Sigma, 6) / pow(r, 7) - 2 * pow(Sigma, 12) / pow(r, 13));
}

double lennord_jones_potential(double r)
{
    double ratio = Sigma / r;
    return 4 * Epsilon * (pow(ratio, 12) - pow(ratio, 6));
}

/* Function the update the positions and velocities. */
void update(Mollist * moll)
{
    int i;
    int j;
    double f, fx, fy;
    double r;
    Distance dis;
    Molecular * m;

    dis.delta_x = 0;
    dis.delta_y = 0;
    dis.rsqure = 0;

    moll->kinetic_energy = 0;
    moll->potential_energy = 0;

    /* Update positions first according to Velocity-Verlet algorithm. */
    for(i=0; i<moll->length; i++)
    {
        m = moll->array[i];

        m->x += velocity_verlet_position(m->vx, m->force_x_old / M);
        /* Perodic boundry. */
        if(m->x > L || m->x <0)
        {
            m->x -= L * floor(m->x / L);
        }

        m->y += velocity_verlet_position(m->vy, m->force_y_old / M);
        if(m->y > L || m->y < 0)
        {
            m->y -= L * floor(m->y / L);
        }

        /* Clear the old values. */
        m->force_x = 0;
        m->force_y = 0;
    }

    /* Update forces and potential energy. */
    for(i=0; i<moll->length; i++)
    {
        for(j=i+1; j<moll->length; j++)
        {

            find_distance(moll->array[i], moll->array[j], &dis);
            r = sqrt(dis.rsqure);
            f = lennord_jones_force(r);
            moll->potential_energy += lennord_jones_potential(r);

            fx = f * dis.delta_x / dis.rsqure;
            fy = f * dis.delta_y / dis.rsqure;

            moll->array[i]->force_x += fx;
            moll->array[i]->force_y += fy;

            moll->array[j]->force_x -= fx;
            moll->array[j]->force_y -= fy;
        }
    }

    /* Update velocities and kinetic energy. */
    for(i=0; i<moll->length; i++)
    {
        m = moll->array[i];

        m->vx += velocity_verlet_velocity(m->force_x_old, m->force_x / M);
        m->force_x_old = m->force_x;

        m->vy += velocity_verlet_velocity(m->force_y_old, m->force_y / M);
        m->force_y_old = m->force_y;

        moll->kinetic_energy += 0.5 * M * (m->vx * m->vx + m->vy * m->vy);
    }

}

double velocity_verlet_position(double velocity, double acceleration)
{
    return velocity * Delta_t + 0.5 * acceleration * Delta_t * Delta_t;
}

double velocity_verlet_velocity(double old_a, double new_a)
{
    return 0.5 * Delta_t * (old_a + new_a);
}
