#include <stdlib.h>
#include <stdio.h>
#include <mds.h>
#include <time.h>
#include <math.h>

int main (int argc, char *argv[])
{
    int i, j;
    srand(time(NULL)); 
    Mollist * moll = Mollist_create(N);
    Mollist_init(moll);
    FILE * fp;

    Molecular * m;
    double vsqure;
    
    fp = fopen("./data/velocity.txt", "w");

    for(j=0; j<STEPS; j++)
    {
        printf("%d\n", j);

        for(i=0; i<moll->length; i++)
        {
            m = moll->array[i];
            vsqure = m->vx * m->vx + m->vy * m->vy;
            fprintf(fp, "%f ", sqrt(vsqure));
        }
        fprintf(fp, "\n");

        update(moll);
    }

    Mollist_destroy(moll);
    fclose(fp);
    return EXIT_SUCCESS;
}
