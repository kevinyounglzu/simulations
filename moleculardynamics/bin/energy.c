#include <stdlib.h>
#include <stdio.h>
#include <mds.h>
#include <time.h>

int main (int argc, char *argv[])
{
    int i, j;
    srand(time(NULL)); 
    Mollist * moll = Mollist_create(N);
    Mollist_init(moll);
    FILE * fp;

    Molecular * m;
    
    fp = fopen("./data/energy.txt", "w");


    for(j=0; j<STEPS; j++)
    {
        printf("%d\n", j);

        fprintf(fp, "%f %f\n", moll->kinetic_energy, moll->potential_energy);

        update(moll);
    }

    Mollist_destroy(moll);
    fclose(fp);
    return EXIT_SUCCESS;
}
